﻿using NUnit.Framework;
using OhmValueCalculator.Services;

namespace OhmValueCalculator.Tests.Services
{
    [TestFixture]
    public class OhmValueServiceTests
    {
        IOhmValueService _service;

        [OneTimeSetUp]
        public void TestInitialize()
        {
            _service = new OhmValueService();
        }

        [TestCase("", "", "", "", ExpectedResult = 0)]
        [TestCase("Brown", "", "", "", ExpectedResult = 0)]
        [TestCase("Brown", "Red", "", "", ExpectedResult = 0)]
        [TestCase("Brown", "Red", "Black", "", ExpectedResult = 12)]
        [TestCase("Brown", "Red", "Black", "Gold", ExpectedResult = 12)]
        [TestCase("Blue", "Green", "Yellow", "Gold", ExpectedResult = 650000)]
        public int AllBlankTest(string bandA, string bandB, string bandC, string bandD)
        {
            return _service.CalculateOhmValue(bandA, bandB, bandC, bandD);
        }
    }
}
