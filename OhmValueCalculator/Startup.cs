﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OhmValueCalculator.Startup))]
namespace OhmValueCalculator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
