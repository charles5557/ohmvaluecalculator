using OhmValueCalculator.Controllers;
using OhmValueCalculator.Interfaces;

namespace OhmValueCalculator.Models
{
    public class OhmValueCalculator : IOhmValueCalculator
    {
        private readonly IColorDecoder _colorDecoder;

        public OhmValueCalculator(IColorDecoder colorDecoder = null)
        {
            _colorDecoder = colorDecoder ?? new ColorDecoder();
        }

        public int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            var firstSignificantFigure = _colorDecoder.SignificantFigures(bandAColor);
            var secondSignificantFigure = _colorDecoder.SignificantFigures(bandBColor);
            var decimalMultiplier = _colorDecoder.Multiplier(bandCColor);
            var tolerancePercentage = _colorDecoder.TolerancePercentage(bandDColor);
            return (int)((firstSignificantFigure * 10 + secondSignificantFigure) * decimalMultiplier);
        }
    }
}