using System.Collections.Generic;
using System.Web.Mvc;

namespace OhmValueCalculator.Models
{
    public class OhmValueViewModel
    {
        public readonly IEnumerable<SelectListItem> BandAItems;
        public readonly IEnumerable<SelectListItem> BandBItems;
        public readonly IEnumerable<SelectListItem> BandCItems;
        public readonly IEnumerable<SelectListItem> BandDItems;
        public string BandAValue { get; set; }
        public string BandBValue { get; set; }
        public string BandCValue { get; set; }
        public string BandDValue { get; set; }

        public OhmValueViewModel()
        {
            BandAItems = new List<SelectListItem>();
            BandBItems = new List<SelectListItem>();
            BandCItems = new List<SelectListItem>();
            BandDItems = new List<SelectListItem>();
        }

        public OhmValueViewModel(IEnumerable<SelectListItem> standBandValues, IEnumerable<SelectListItem> standardToleranceValues)
        {
            BandAItems = standBandValues;
            BandBItems = standBandValues;
            BandCItems = standBandValues;
            BandDItems = standardToleranceValues;
        }

        public OhmValueViewModel(IEnumerable<SelectListItem> bandAItems, IEnumerable<SelectListItem> bandBItems, IEnumerable<SelectListItem> bandCItems, IEnumerable<SelectListItem> bandDItems)
        {
            BandAItems = bandAItems;
            BandBItems = bandBItems;
            BandCItems = bandCItems;
            BandDItems = bandDItems;
        }
    }
}