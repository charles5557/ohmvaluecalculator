using System.Collections.Generic;
using System.Linq;
using OhmValueCalculator.Interfaces;
using OhmValueCalculator.Models;

namespace OhmValueCalculator.Controllers
{
    public class ColorDecoder: IColorDecoder
    {
        private readonly IEnumerable<ColorCode> _colorCodes = new List<ColorCode>
        {
            new ColorCode {Color = "Black", SignificantFigures = 0},
            new ColorCode {Color = "Brown", SignificantFigures = 1, TolerancePercentage = 1},
            new ColorCode {Color = "Red", SignificantFigures = 2, TolerancePercentage = 2},
            new ColorCode {Color = "Orange", SignificantFigures = 3},
            new ColorCode {Color = "Yellow", SignificantFigures = 4, TolerancePercentage = 5},
            new ColorCode {Color = "Green", SignificantFigures = 5, TolerancePercentage = .5},
            new ColorCode {Color = "Blue", SignificantFigures = 6, TolerancePercentage = .25},
            new ColorCode {Color = "Violet", SignificantFigures = 7,TolerancePercentage = .1},
            new ColorCode {Color = "Gray", SignificantFigures = 8, TolerancePercentage = .05},
            new ColorCode {Color = "White", SignificantFigures = 9},
            new ColorCode {Color = "Gold",SignificantFigures=-1, TolerancePercentage = 5},
            new ColorCode {Color = "Silver",SignificantFigures=-1, TolerancePercentage = 10}
        };

        public int SignificantFigures(string color)
        {
            return string.IsNullOrEmpty(color) ? 0 : _colorCodes.Single(colorCode => colorCode.Color.Equals(color)).SignificantFigures;
        }

        public double TolerancePercentage(string color)
        {
            return string.IsNullOrEmpty(color) ? 0.2 : _colorCodes.Single(colorCode => colorCode.Color.Equals(color)).TolerancePercentage / 100.00;
        }

        public double Multiplier(string color)
        {
            return string.IsNullOrEmpty(color) ? 0 : _colorCodes.Single(colorCode => colorCode.Color.Equals(color)).Multiplier;
        }

        public IEnumerable<ColorCode> GetColorCodes()
        {
            return _colorCodes;
        }
    }
}