﻿using System;

namespace OhmValueCalculator.Models
{
    public class ColorCode
    {
        public string Color { get; set; }
        public int SignificantFigures { get; set; }
        public double Multiplier => SignificantFigures == -1 ? 1 : Math.Pow(10, SignificantFigures);
        public double TolerancePercentage { get; set; }
    }
}