﻿using System.Web.Mvc;
using OhmValueCalculator.Models;
using OhmValueCalculator.Services;

namespace OhmValueCalculator.Controllers
{
    public class HomeController : Controller
    {
        readonly IOhmValueService _ohmValueService = new OhmValueService();

        public ActionResult Index()
        {
            var viewModel = new OhmValueViewModel(_ohmValueService.GetStandardBandValues(),_ohmValueService.GetStandardToleranceValues());
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult CalculateOhmValue(OhmValueViewModel model)
        {
            var value = _ohmValueService.CalculateOhmValue(model.BandAValue, model.BandBValue, model.BandCValue, model.BandDValue);
            return Json(new { value });
        }
    }
}