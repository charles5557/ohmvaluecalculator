using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OhmValueCalculator.Controllers;
using OhmValueCalculator.Interfaces;
using OhmValueCalculator.Models;

namespace OhmValueCalculator.Services
{
    public class OhmValueService: IOhmValueService
    {
        private readonly IOhmValueCalculator _calculator;
        private readonly IColorDecoder _colorDecoder;

        public OhmValueService(ColorDecoder colorDecoder=null, IOhmValueCalculator calculator = null)
        {
            _colorDecoder = colorDecoder??new ColorDecoder();
            _calculator = calculator ?? new Models.OhmValueCalculator(_colorDecoder);
        }

        public int CalculateOhmValue(string bandAValue, string bandBValue, string bandCValue, string bandDValue)
        {
            return _calculator.CalculateOhmValue(bandAValue, bandBValue, bandCValue, bandDValue);
        }

        public IEnumerable<SelectListItem> GetStandardBandValues()
        {
            return _colorDecoder.GetColorCodes().Select(color => new SelectListItem
            {
                Text = color.Color
            });
        }

        public IEnumerable<SelectListItem> GetStandardToleranceValues()
        {
            double TOLERANCE = 0;
            return _colorDecoder.GetColorCodes().Where(color => Math.Abs(color.TolerancePercentage) > TOLERANCE).Select(color => new SelectListItem
            {
                Text = color.Color
            });
        }
    }

    public interface IOhmValueService
    {
        int CalculateOhmValue(string bandAValue, string bandBValue, string bandCValue, string bandDValue);
        IEnumerable<SelectListItem> GetStandardBandValues();
        IEnumerable<SelectListItem> GetStandardToleranceValues();
    }
}