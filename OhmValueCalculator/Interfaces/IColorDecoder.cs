using System.Collections.Generic;
using OhmValueCalculator.Models;

namespace OhmValueCalculator.Interfaces
{
    public interface IColorDecoder
    {
        int SignificantFigures(string color);
        double TolerancePercentage(string color);
        double Multiplier(string color);
        IEnumerable<ColorCode> GetColorCodes();
    }
}